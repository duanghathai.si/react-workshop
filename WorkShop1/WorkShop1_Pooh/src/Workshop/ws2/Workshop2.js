import { useState } from "react";
import "./ws2.css";


function Workshop2() {
  //   const [count, setCount] = useState(0);
  //   const [student, setStudent] = useState([
  //     { id: 1, name: "Pooh" },
  //     { id: 2, name: "อิอิ" },
  //     { id: 3, name: "wow" },
  //   ]);

  //   function deleteStudent(id) {
  //     setStudent(student.filter((item) => item.id !== id));
  //   }

  //   const [show, setShow] = useState(true);
  const [number1, setNumber1] = useState();
  const [number2, setNumber2] = useState();
  const [operator, setOperator] = useState();
  const [cal, setCal] = useState();

  function calculate(num1, ope, num2) {
    if (ope === "+") {
      setCal((parseFloat(num1) + parseFloat(num2)).toFixed(3));
    }

    if (ope === "-") {
      setCal((parseFloat(num1) - parseFloat(num2)).toFixed(3));
    }

    if (ope === "*") {
      setCal((parseFloat(num1) * parseFloat(num2)).toFixed(3));
    }

    if (ope === "/") {
      setCal((parseFloat(num1) / parseFloat(num2)).toFixed(3));
    }
  }

  return (
    <div className="content2">
      {/* <h1>{count}</h1>
      <button onClick={() => setCount(count + 1)}>+</button>
      <button onClick={() => setCount(count - 1)}>-</button>
      <button onClick={() => setCount(0)}>reset</button>

      <br />
      <h1> std length = {student.length}</h1>
      <button onClick={() => setShow(!show)}>toggle</button>
      <ul>
        {show &&
          student.map((item) => (
            <li key={item.id}>
              <p>
                {item.id} - {item.name}
              </p>
              <button onClick={() => deleteStudent(item.id)}>delete</button>
            </li>
          ))}
      </ul> */}
      <br />
      <div className="data">
        <p> Number 1 : </p>
        <input
          type="float"
          value={number1}
          onChange={(e) => setNumber1(e.target.value)}
        />
        <p> Parameter : </p>
        <select
          name="operators"
          onChange={(e) => setOperator(e.target.value)}
          value={operator}
          
          required
        >
          <option value="+">+</option>
          <option value="-">-</option>
          <option value="*">*</option>
          <option value="/">/</option>
        </select>
        <p> Number 2 </p>
        <input
          type="float"
          value={number2}
          style={{marginBottom:"20px"}}
          onChange={(e) => setNumber2(e.target.value)}
          required
        />{" "}
        <br />
        <button style={{marginBottom:"50px"}} onClick={() => calculate(number1, operator, number2)}>
          Calculate
        <br />
        </button>
        {" "} = {cal}
      </div>
    </div>
  );
}

export default Workshop2;
