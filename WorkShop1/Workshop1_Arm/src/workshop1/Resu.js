import Profile from '../image/profile.png';
function Resu() {
  return (
    <div
      style={{
        position: "absolute",
        top: "50%",
        left: "50%",
        transform: "translate(-50%, -50%)",
        marginTop: "100px",
        width: "70%",
      }}
    >
      <div style={{ textAlign: "center" }}>
        <img
          src={Profile}
          alt="Profile"
          style={{ width: "200px", borderRadius: "50%" }}
        />
        <h1>ชื่อ: จิฏฏิภัค พลหงษ์</h1>
      </div>
      <h3>Internship Front end developer</h3>
      <h4>ชื่อเล่น: อาม</h4>
      <h4>ข้อมูลส่วนตัว</h4>
      <ul>
        <li>อายุ: 21 ปี</li>
        <li>เบอร์โทร: 098-587-6978</li>
        <li>อีเมล์: jittipak.p@kkumail.com</li>
      </ul>
      <h4>การศึกษา</h4>
      <ul>
        <li>
          <strong>ปริญญาตรี:</strong> มหาวิทยาลัยขอนเเก่น -
          สาขาวิชาวิทยาการคอมพิวเตอร์
        </li>
      </ul>
      <h4>Skill</h4>
      <ul>
        <li>html</li>
        <li>css</li>
        <li>javascript</li>
        <li>python</li>
      </ul>
      <h4>งานอดิเรก</h4>
      <ul>
        <li>เป็นอาสาสมัคร</li>
        <li>เล่นกีต้าร์</li>
        <li>เล่นบาส</li>
      </ul>
    </div>
  );
}

export default Resu;
