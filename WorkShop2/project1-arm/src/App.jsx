import Header from "./components/Header";
import AddForm from "./components/AddForm";
import Item from "./components/Item";
import "./App.css";
import { useState, useEffect } from "react";

function App() {
  const [tasks, setTask] = useState(
    JSON.parse(localStorage.getItem("tasks")) || []
  );
  const [title, setTitle] = useState("");
  const [editID, setEditid] = useState(null);
  const [theme, setTheme] = useState("light");
  useEffect(() => {
    localStorage.setItem("tasks", JSON.stringify(tasks));
  }, [tasks]);

  function delTask(id) {
    const result = tasks.filter((item) => item.id !== id);
    setTask(result);
  }
  function editTask(id) {
    setEditid(id);
    const editTask = tasks.find((item) => item.id === id);
    setTitle(editTask.title);
  }
  function saveTask(e) {
    e.preventDefault();
    console.log(title);
    if (!title) {
      alert("ยังไม่ได้ป้อนข้อมูล");
    } else if (editID) {
      const updeteTask = tasks.map((item) => {
        if (item.id === editID) {
          return { ...item, title: title };
        }
        return item;
      });
      setTask(updeteTask);
      setEditid(null);
      setTitle("");
    } else {
      const newTask = {
        id: Math.floor(Math.random() * 1000),
        title: title,
      };
      setTask([...tasks, newTask]);
      setTitle("");
    }
  }
  return (
    <div className={"App " + theme}>
      <Header theme={theme} setTheme={setTheme} />
      <div className="container">
        <AddForm
          title={title}
          setTitle={setTitle}
          saveTask={saveTask}
          editID={editID}
        />
        <section>
          {tasks.map((data) => (
            <Item
              key={data.id}
              data={data}
              delTask={delTask}
              editTask={editTask}
            />
          ))}
        </section>
      </div>
    </div>
  );
}

export default App;
