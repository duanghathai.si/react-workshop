import * as React from "react";
import { useParams } from "react-router-dom";
import { useState, useEffect } from "react";
import CssBaseline from "@mui/material/CssBaseline";
import Container from "@mui/material/Container";
import { TextField, Typography } from "@mui/material";
import Grid from "@mui/material/Grid";
import Button from "@mui/material/Button";

const urlApi = "https://735c-110-170-188-98.ngrok-free.app/customer";

export default function UserEdit() {
  const { id } = useParams();
  useEffect(() => {
    console.log("Edit at id :" + id);
    var requestOptions = {
      method: "GET",
      redirect: "follow",
    };

    fetch(urlApi + "/" + id, requestOptions)
      .then((response) => response.json())
      .then((result) => {
        console.log(result);
        if (result !== "") {
          setName(result["Customer_name"]);
          setTel(result["Phone_number"]);
          setBirthDate(result["Birth_date"]);
        }
      })
      .catch((error) => console.log("error", error));
  }, [id]);

  const handleSubmit = (event) => {
    console.log("HandleSubmit is runing");
    event.preventDefault();
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    var raw = JSON.stringify({
      Customer_id: id,
      Customer_name: name,
      Phone_number: tel,
      Birth_date: birthDate,
    });

    var requestOptions = {
      method: "PUT",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };

    fetch(urlApi, requestOptions)
      .then((response) => response.text())
      .then((result) => {
        console.log(result);
        alert(result);
        if (result === "Update Customer Complete") {
          window.location.href = "/";
        }
      })
      .catch((error) => console.log("error", error));
  };
  const [name, setName] = useState("");
  const [tel, setTel] = useState("");
  const [birthDate, setBirthDate] = useState("");

  return (
    <React.Fragment>
      <CssBaseline />
      <Container maxWidth="sm" sx={{ p: 2 }}>
        <Typography variant="h6" gutterBottom component="div">
          Update User
        </Typography>
        <form onSubmit={handleSubmit}>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <TextField
                id="name"
                label="Name"
                variant="outlined"
                fullWidth
                required
                onChange={(e) => setName(e.target.value)}
                value={name}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                id="tel"
                label="Phone Number"
                variant="outlined"
                fullWidth
                required
                onChange={(e) => setTel(e.target.value)}
                value={tel}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                id="birthDate"
                label="Birth Date"
                variant="outlined"
                fullWidth
                required
                onChange={(e) => setBirthDate(e.target.value)}
                value={birthDate}
              />
            </Grid>
            <Grid item xs={12}>
              <Button type="submit" variant="contained" fullWidth>
                Update
              </Button>
            </Grid>
          </Grid>
        </form>
      </Container>
    </React.Fragment>
  );
}
