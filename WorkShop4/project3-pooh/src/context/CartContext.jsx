import { createContext, useContext, useReducer, useEffect } from "react";
import products from "../data/product";
import cartReducer from "../reducer/cartReducer";

const CartContext = createContext();
const initState = {
  products: products,
  total: 0,
  amount: 0,
};

export const CartProvider = ({ children }) => {
  const [state, dispatch] = useReducer(cartReducer, initState);

  function formatMoney(money) {
    return money.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
  }

  function addQuantity(id) {
    console.log("Add product ID : " + id);
    dispatch({ type: "ADD", payload: id });
  }

  function subtractQuantity(id) {
    console.log("Subtract product amount = " + id);
    dispatch({ type: "SUBTRACT", payload: id });
  }

  function removeItem(id) {
    dispatch({ type: "REMOVE", payload: id });
    console.log("Delete product ID = " + id);
  }

  useEffect(() => {
    dispatch({ type: "CALCULATE_TOTAL" });
  }, [state.products]);

  return (
    <CartContext.Provider
      value={{
        ...state,
        formatMoney,
        removeItem,
        addQuantity,
        subtractQuantity,
      }}
    >
      {children}
    </CartContext.Provider>
  );
};

export const useCart = () => {
  return useContext(CartContext);
};
